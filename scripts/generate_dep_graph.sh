#!/bin/bash
mkdir -p ../build
mkdir -p ../graphviz

cd ../build
cmake -DGRAPHVIZ_EXECUTABLES=FALSE -DGRAPHVIZ_SHARED_LIBS=FALSE --graphviz=../graphviz/graph .
dot ../graphviz/graph -T png -o ../graphviz/graph.png

